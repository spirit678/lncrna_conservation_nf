# Nextflow pipeline for defining lncRNA conservation
* #### Overview
* #### Installation
* #### Usage
   * #### identifing of non-coding RNAs
   * #### identifing conserved transcripts
* #### Results
* #### Components
* #### Cite

## Overview
Here we present [nextflow](https://www.nextflow.io/) based bioinformatics workflow for evolutionary analyses of lncRNAs. It consists of two parts. The first part enables ab initio transcriptome assembly from RNA-Seq data, followed by identification of lncRNAs, using a number of commonly accepted criteria. The second module, based on [slncky](https://slncky.github.io/index.html) and a set of Python and R scripts, serves as a tool for identification of conserved lncRNAs, utilizing both synteny and sequence identities across species of interest.
Using different implementation of the same approach we previously developed [SyntDB](http://syntdb.amu.edu.pl/) - a database of human long non-coding RNAs conserved across primates. The goal of the current pipeline implementation is to make conservation analysis easier for people with limited knowledge in area of bioinformatics in the meantime utilizing all the [benefits](https://www.nextflow.io/index.html#Features) of using Nextflow.


## Installation
1. #### Docker
    1. Check if docker instelled in your system
    ```bash
    docker -v
    ```
    1. If not - follow docker [installation guide](https://docs.docker.com/install/)
    1. Do not forget to add your user to the docker group.
    ```bash
    sudo groupadd docker && sudo usermod -aG docker $USER
    ```
1. #### Nextflow
    1. Make sure java 8 or later is installed on your computer and run
    ```bash
    java -version  
    ```
    2. Install nextflow (it creates a file nextflow in the current dir)
   ```bash
   curl -s https://get.nextflow.io | bash
   ```
    1. To avoid remembering and typing the full path to nextflow each time you need to run it
    ```bash
    sudo cp -s ./nextflow /usr/local/bin    
    ```
1. #### Pipeline
    1. Clone the repository
    ```bash
    git clone https://gitlab.com/spirit678/lncrna_conservation_nf.git
    ```
    2. Go to the folder
    ```bash
    cd ./lncrna_conservation_nf
    ```
    3. (optional) Build the docker container with all the software  
    ```bash
    docker build -t lncrna_pipe .
    ```
    4. (optional) To check container run:
    ```bash
    docker run -i lncrna_pipe
    ```
    use `exit` to log out from container

## Usage
There are two main types of tasks: lncRNA identification from RNAseq and conservation analysis; To run conservation analysis you need to have a sets of lncRNAs obtained from previous step for both species.

By default the pipeline is parallelized by spanning multiple threads in the machine where the script is launched.
####  Identification of non-coding RNAs  [assembly DAG](https://diffsuff.s3.amazonaws.com/flowchart.html)
  1. Input data. As an input data pipeline require reads in the folder `./Data/your_reads` in format `*_R{1,2}.fastq.gz` and the name of the species, wich will be used to fetch a fresh annotations from ENSEMBL
  1. We use biomart to find a right annotations on ENSEMBL, so for human queryies like `Human` or `GRCh38.p13` or `hsapiens` will be fine, after we use REST API to actually fetch the data.
  1. RUN the workflow with minimal settings  
  ```bash
  nextflow run ./main.nf --main assembly --species Human
  ```
  Results will be stored in the `./Data` folder. In case of human results of the analysis will be stored in `./Data/hsapiens`

#### Identification of conserved transcripts [conservation DAG](https://diffsuff.s3.amazonaws.com/flowchart_2.html)
1.  Input data.As an input data for conservation analysis lncRNAs for query and target species discovered on the previous step should be located in the folder `./Data/species_name`.
1. Cross-species conservation analysis required two species, target (reference source of lncRNAs) and query (where to look for conserved counterparts), should be defined as  `--target_species` and `--query_species` respectively. Also metrics reflected evolutionary distance should be defined under `--distance` option and may be "near", "medium" and "far"
  ```bash
  nextflow run ./main.nf --main conservation --target_species hsapiens --query_species ptroglodytus \
  --distance near
  ```
#### (optional) Analytical step
1. Comparison of assembled noncoding transcriptomes of two species
  ```bash
  nextflow run ./main.nf --main comparison --data_type assembly --species1 hsapiens --species2 ecaballus
  ```
2. Comparison of conservation analysis between 2 pairs of species 
  ```bash
  nextflow run ./main.nf --main comparison --data_type conservation --target1 hsapiens --query1 ecaballus --target2 hsapiens --query2 ptroglodytes
  ```
3. Resulted data are stored within `/Data/` directory. Example of assembly comparison [hsapiens-ecaballus](https://diffsuff.s3.amazonaws.com/hsapiens_ecaballus_lncRNAS.html) and conservation [hsapiens-ecaballus vs hsapiens-ptroglodytes](https://diffsuff.s3.amazonaws.com/hsapiens-ecaballus_hsapiens-ptroglodytes_conservation.html)
## AWS Batch
Note that you need to create s3://your_nextflow_dir with subfolders `Data` and `work`. In my case it called `nextflowdir`
Assembly:
  ```bash
  ./nextflow run ./main.nf -profile awsbatch --reads "s3://nextflowdir/Data/your_reads/*_R{1,2}.fastq.gz" \ 
  --outdir "s3://nextflowdir/Data" -bucket-dir  s3://nextflowdir/work --main assembly --species "Human" 
  ```
Conservation:
  ```bash
  ./nextflow run ./aws_batch.nf -profile awsbatch --outdir "s3://nextflowdir/Data" -bucket-dir  s3://nextflowdir/work \
  --main conservation --target_species hsapiens --query_species ptroglodytes --distance near
  ```
To use AWS Athena you have to add  `--analytics yes` to the conservation stage.
  ```bash
  ./nextflow run ./aws_batch.nf -profile awsbatch --outdir "s3://nextflowdir/Data" -bucket-dir  s3://nextflowdir/work \
  --main conservation --target_species hsapiens --query_species ptroglodytes --distance near --analytics yes
  ```
 The data schema is described within `Athena_schema.txt`
## Results
#### Run report and DAG
Nextflow can create an [HTML execution report](https://diffsuff.s3.amazonaws.com/report.html): a single document which includes many useful metrics about a workflow execution. The report is organised in the three main sections: Summary, Resources and Tasks.
A Nextflow pipeline is implicitly modelled by a [direct acyclic graph](https://diffsuff.s3.amazonaws.com/flowchart.html) (DAG). The vertices in the graph represent the pipeline's processes and operators, while the edges represent the data connections (i.e. channels) between them.(disable by default, to enable add `-with-dag flowchart.html`)
#### Quality control for transcriptome assembly
For assembly stage we provide fastq quality reports([fastqc_R1](https://diffsuff.s3.amazonaws.com/ENCLB030AMW_R1_fastqc.html) and [fastqc_R2](https://diffsuff.s3.amazonaws.com/ENCLB030AMW_R2_fastqc.html)) also we provide [fastp](https://diffsuff.s3.amazonaws.com/ENCLB030AMW_fastp.html) report for each sample and the [multiqc](https://diffsuff.s3.amazonaws.com/multiqc_report.html) report for the whole assembly.

#### Results for transcriptome assembly
Assembled transcriptome located in the  `./Data/species_name` directory and represented by  `species_name_final.fasta`  `species_name_final.gtf`. Filtered lncRNAs located in the `./Data/species_name/PREDICTIONS/` and represented by 3 files: `species_name_lncrnas_data.txt` `species_name_lncRNAs.gtf`, `species_name_lncRNAs.fasta`


Example of the output folder for `hsapiens` can be downloaded from [here](https://diffsuff.s3.amazonaws.com/hsapiens.tar.xz)

#### Results for conservation analysis
For each pair of target-query species our pipeline produce two files: `${target}.${query}.orthologs.top.final.txt` and `${target}.${query}.orthologs.final.txt` located in the `./Data/query_species_name/` 

## Components
  - [slncky 1.0](https://slncky.github.io/)
  - [sweetviz](https://github.com/fbdesignpro/sweetviz)
##### Python
  - biopython=1.74
  - pybiomart=0.2.0
  - pandas=1.0.1
  - matplotlib=3.1.1

##### bioconda packages
  - cufflinks=2.2.1
  - fastqc=0.11.8
  - bbmap=38.73
  - samtools=1.9
  - star=2.7.3a             
  - stringtie=2.0
  - salmon=0.14.2
  - transdecoder=5.5.0
  - blast-legacy=2.2.26
  - bowtie2=2.3.5
  - multiqc=1.8
  - bedtools=2.24.0
  - cpc2=2.0b
  - lastz=1.0.4
  - last=1061
  - fastp=0.20.0
  - ucsc-liftover=366
  - ucsc-fatotwobit=366
  - ucsc-twobitinfo=366
  - ucsc-axtchain=366
  - ucsc-chainmergesort=366
  - ucsc-chainnet=366
  - ucsc-lavtopsl=366
  - ucsc-netchainsubset=366


