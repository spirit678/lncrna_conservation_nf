#!/usr/bin/env python3
# coding: utf-8

# In[1]:


import os
import pandas as pd


# In[44]:


samples=[os.path.join('.', o) for o in os.listdir(".") 
                    if os.path.isdir(os.path.join(".",o))]
#samples.remove('./.ipynb_checkpoints')
df= pd.DataFrame()
for i in samples:
    temp = pd.read_csv(str(i +'/quant.sf'), sep="\t")
    temp = temp[["Name", "TPM"]]
    temp.rename(columns={'Name':'Transcript', "TPM":str(i.split("/")[1].split("_")[0])}, inplace=True)
    temp.set_index('Transcript', inplace=True)
    #print(temp.head(5))
    #print(str(i.split("/")[1].split("_")[0]), temp.shape)
    if df.empty == True:
        df = temp
    else:
        
        df = pd.concat([df, temp.reindex(df.index)], axis=1)
df['max'] = df.max(axis=1)


# In[46]:


df.to_csv('expression.tsv', sep="\t", index=True)

