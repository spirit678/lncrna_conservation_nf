#!/usr/bin/env python2.7
import sys

experiment = sys.argv[1]

FASTA = 'FASTA/' + experiment + '_pre-filtered.fasta'
transdecoder = 'TRANSDECODER/' + experiment + '_pre-filtered.fasta.transdecoder_dir/longest_orfs.pep'

f1 = open(FASTA)
f2 = open(transdecoder)

out = open(experiment + '_pre-filtered_2.fasta', 'w')


dict_seq = {}
for line in f1:
  if line[0] == '>':
    id = line[1:].split(' ')[0].strip()
  else:
    seq = line.strip()
    if id in dict_seq:
      dict_seq[id] += seq
    else:
      dict_seq[id] = seq



dict_discard = {}
for line in f2:
  l = line
  if line[0] == '>':
    transcript = '.'.join(line[1:].split(' ')[0].split('.')[:-1])
    if 'type:complete' in l:
      dict_discard[transcript] = ''


for transcript, seq in dict_seq.items():
  if not transcript in dict_discard:
    print "+",
    write = '>' + transcript + '\n' + seq + '\n'
    out.write(write)
  else:
    print "-",

