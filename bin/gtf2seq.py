#!/usr/bin/env python
import sys


from Bio.Seq import Seq
from Bio import SeqIO
from Bio import Alphabet



genome = sys.argv[1]
input = sys.argv[2]
output = sys.argv[3]
output2 = sys.argv[4]


f = open(input)
f2 = open(genome)

out = open(output, 'w')
out2 = open(output2, 'w')

feature_names = ['gene_id', 'transcript_id', 'exon_number', 'gene_name', 'nearest_ref', 'class_code']

dict_transcripts = {}
dict_exon_number = {}
dict_exons = {}
dict_gene_transcript = {}

#print ("Reading GTF file...")

for line in f:
  line = line.strip().split('\t')
  dict_features = {}
  feature_values = []
  chr = line[0]
  feature = line[2]
  start = line[3]
  end = line[4]
  strand = line[6]
  features = line[8].split('; ')
  gene_id = features[0].replace('gene_id ', '').replace('"', '')
  transcript_id = features[1].replace('transcript_id ', '').replace('"', '').replace(';', '')

  if feature == 'exon':
    if gene_id in dict_gene_transcript:
      dict_gene_transcript[gene_id].append(transcript_id)
    else:
      dict_gene_transcript[gene_id] = [transcript_id]
    for feature in features:
      feature = feature.split(' ')
      dict_features[feature[0]] = feature[1].replace('"', '')
    for feature_name in feature_names:
      if feature_name in dict_features:
        feature_value = dict_features[feature_name]
      else:
        feature_value = 'none'
      feature_values.append(feature_value.replace(';', ''))
    if chr in dict_transcripts:
      if feature_values[1] != 'none':
        dict_transcripts[chr].append(feature_values[1])
    else:
      if feature_values[1] != 'none':
        dict_transcripts[chr] = [feature_values[1]]
    if feature_values[1] in dict_exon_number:
      dict_exon_number[feature_values[1]].append(int(feature_values[2]))
    else:
      dict_exon_number[feature_values[1]] = [int(feature_values[2])]
    dict_exons[feature_values[1] + '*' + feature_values[2]] = (chr, start, end, strand, feature_values[0], feature_values[1], feature_values[2], feature_values[3], feature_values[4], feature_values[5])
	
    
for gene, transcripts in dict_gene_transcript.items():
  transcripts = set(transcripts)
  for transcript in transcripts:
    write = gene + '\t' + transcript + '\n'
    out2.write(write)
    
#print ("Extracting sequences...")

seq = ''
for line in f2:
  if '>' in line:
    if seq:
      if this_chr in dict_transcripts:
        transcripts = set(dict_transcripts[this_chr])
        for transcript in transcripts:
          transcript_sequence = ''
          exon_number = int(max(dict_exon_number[transcript]))
          for i in range(exon_number):
            nr = i + 1
            data = dict_exons[transcript + '*' + str(nr)]
            chr, start, end, strand, gene_id, transcript_id, exon_number, gene_name, nearest_ref, class_code = data
            exon_seq = seq[int(start)-1:int(end)] # because coordinates in GTF are 1-based
            if strand == '-':
              #exon_seq = reverse(complement(exon_seq))
              exon_seq = Seq(exon_seq)
              exon_seq = str(exon_seq.reverse_complement())
            if strand == '+':
              transcript_sequence += exon_seq
            if strand == '-':
              transcript_sequence = exon_seq + transcript_sequence
          write = '>' + transcript_id + ' ' + gene_id + '|' + gene_name + '|' + strand + '|' + nearest_ref + '|' + class_code + '\n' + transcript_sequence + '\n'
          if strand in ['+', '-']:  # sometimes strand is unknown, hence there is no sequence extracted
            out.write(write)
      seq = ''
    this_chr = line.strip().split(' ')[0].replace('>', '')
#    print ("chr:", this_chr)
  else:
    line = line.strip()
    seq += line
	
	
if this_chr in dict_transcripts:
#  print ("chr:", this_chr)
  transcripts = set(dict_transcripts[this_chr])
  for transcript in transcripts:
    transcript_sequence = ''
    exon_number = int(max(dict_exon_number[transcript]))
    for i in range(exon_number):
      nr = i + 1
      data = dict_exons[transcript + '*' + str(nr)]
      chr, start, end, strand, gene_id, transcript_id, exon_number, gene_name, nearest_ref, class_code = data
      exon_seq = seq[int(start)-1:int(end)] # because coordinates in GTF are 1-based
      if strand == '-':
        exon_seq = Seq(exon_seq)
        exon_seq = str(exon_seq.reverse_complement())
      if strand == '+':
        transcript_sequence += exon_seq
      if strand == '-':
        transcript_sequence = exon_seq + transcript_sequence
    write = '>' + transcript_id + ' ' + gene_id + '|' + gene_name + '|' + strand + '|' + nearest_ref + '|' + class_code + '\n' + transcript_sequence + '\n'
    if strand in ['+', '-']:  # sometimes strand is unknown, hence there is no sequence extracted
      out.write(write)
seq = ''

