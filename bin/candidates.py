#!/usr/bin/env python2.7
import sys

experiment = sys.argv[1]


dict_class_code_descriptions = {'=':'Complete match of intron chain', 'j':'Potentially novel isoform (fragment): at least one splice junction is shared with a reference transcript', 'i':'A transfrag falling entirely within a reference intron', 'o':'Generic exonic overlap with a reference transcript', 'r':'Repeat', 'u':'Unknown, intergenic transcript', 'x':'Exonic overlap with reference on the opposite strand'}

FASTA = sys.argv[2] #'FASTA/' + experiment + '_pre-filtered_2.fasta'  # to get allowed ids and their sequences
FASTA2 = sys.argv[3] #'FASTA/' + experiment + '_pre-filtered.fasta'  # sequences before TransDecoder and CPC
CPC = sys.argv[4] #'HELPER/' + experiment + '/result_' + experiment
GTF = sys.argv[5] #'FINAL/' + experiment + '.gtf'
COMBINED = sys.argv[6] #'Cuffcompare/' + experiment + '.combined.gtf'
ENSEMBL = sys.argv[7] #'ensembl_data.txt'
expression = sys.argv[8] #'expression.tsv'
exp_treshold = float(sys.argv[9]) # 1.0 TPM

f1 = open(FASTA)
f2 = open(CPC)
f3 = open(GTF)
f4 = open(COMBINED)
f6 = open(ENSEMBL)
f7 = open(FASTA2)


exp = experiment
out1 = open('PREDICTIONS/' + exp + '_lncrnas.fasta', 'w')
out2 = open('PREDICTIONS/' + exp + '_lncrnas_data.txt', 'w')
out3 = open('PREDICTIONS/' + exp + '_lncrnas.gtf', 'w')



dict_seq = {}
for line in f1:
  if line[0] == '>':
    id = line[1:].split(' ')[0].strip()
  else:
    seq = line.strip()
    if id in dict_seq:
      dict_seq[id] += seq
    else:
      dict_seq[id] = seq


dict_seq2 = {}
for line in f7:
  if line[0] == '>':
    id = line[1:].split(' ')[0].strip()
  else:
    seq = line.strip()
    if id in dict_seq2:
      dict_seq2[id] += seq
    else:
      dict_seq2[id] = seq


dict_noncoding = {}
for line in f2:
  line = line.strip().split('\t')
  transcript = line[0]
  result = line[7]
  if result == 'noncoding':
    dict_noncoding[transcript] = ''



print "Getting a list of candidates..."
c = 0
dict_accepted = {}
for id in dict_seq.keys():
  if id in dict_noncoding:
    dict_accepted[id] = ''  # not protein coding according to Transdecoder and CPC



print "Getting expression data..."
dict_TPM = {}
dict_FPKM = {}
dict_transcript_coordinates = {}
dict_exon_coordinates = {}

cex=0
cnex=0
with open(expression , 'r') as f8:
  f8.readline()
  for line in f8:
    line = line.strip().split('\t')
    transcript = line[0]
    max_TPM = line[-1]
    if float(max_TPM) >= exp_treshold:
      dict_TPM[transcript] = max_TPM
      cex+=1
    else:
      cnex+=1
      pass

#print("pass ", cex, " not ", cnex)


dict_coords = {}
dict_strand_chr = {}
for line in open(GTF):
  line = line.strip().split('\t')
  chr = line[0]
  feature = line[2]
  if feature == 'exon':
    start = int(line[3])
    end = int(line[4])
    strand = line[6]
    features = line[8].split('; ')
    transcript, gene = '', ''
    for elem in features:
      if 'transcript_id' in elem:
        transcript = elem.split(' ')[1].replace('"', '').replace(';', '')
      if 'gene_id' in elem and not 'ref' in elem:
        gene = elem.split(' ')[1].replace('"', '')
    if transcript in dict_coords:
      dict_coords[transcript].append(start)
      dict_coords[transcript].append(end)
    else:
      dict_coords[transcript] = [start]
      dict_coords[transcript].append(end)
    dict_strand_chr[transcript] = (strand, chr)


for transcript, t in dict_strand_chr.items():
  strand, chr = t
  coords = dict_coords[transcript]
  start = str(min(coords))
  end = str(max(coords))
  dict_transcript_coordinates[transcript] = (chr, start, end, strand)


for line in f3:
  if not line[0] == '#':
    line = line.strip().split('\t')
    chr = line[0]
    start = line[3]
    end = line[4]
    strand = line[6]
    if line[2] == 'exon':
      start = line[3]
      end = line[4]
      features = line[8].split('; ')
      for elem in features:
        if 'transcript_id' in elem:
          transcript = elem.split(' ')[1].replace('"', '').replace(';', '')
      if transcript in dict_exon_coordinates:
        dict_exon_coordinates[transcript].append(start + '-' + end)
      else:
        dict_exon_coordinates[transcript] = [start + '-' + end]




print "Reading comparison with known annotation..."
dict_exons = {}
dict_class_codes = {}
dict_nearest_ref = {}
dict_length = {}
for line in f4:
  line = line.strip().split('\t')
  features = line[8].split('; ')
  exon_start = int(line[3])
  exon_end = int(line[4])
  exon_length = exon_end - exon_start + 1
  class_code, transcript, nearest_ref = 'NA', 'NA', 'NA'
  for feature in features:
    if 'class_code' in feature:
      class_code = feature.split(' ')[1].replace('"', '')
    if 'oId' in feature:
      transcript = feature.split(' ')[1].replace('"', '')
    if 'nearest_ref' in feature:
      nearest_ref = feature.split(' ')[1].replace('"', '')
      dict_nearest_ref[transcript] = nearest_ref
  dict_class_codes[transcript] = class_code
  if transcript in dict_exons:
    dict_exons[transcript] += 1
  else:
    dict_exons[transcript] = 1
  if transcript in dict_length:
    dict_length[transcript] += exon_length
  else:
    dict_length[transcript] = exon_length



dict_data = {}
dict_data_html = {}
dict_biotype = {}
f6.readline()
for line in f6:
  l = line
  line = line.replace('\n', '').split('\t')
  gene = line[0]
  transcript = line[1]
  protein = line[2]
  description = line[3]
  chr = line[4]
  strand = line[5]
  band = line[6]
  start = line[7]
  end = line[8]
  length = line[9]
  gene_name = line[10]
  gene_type = line[12]
  transcript_type = line[12]
  description = description.split(' [')[0]
  if strand == '-1':
    strand = '-'
  if strand == '1':
    strand = '+'
  l_html = '<a href = http://sep2015.archive.ensembl.org/Homo_sapiens/Gene/Summary?db=core;g=' + gene + '>' + gene + '</a>\t<a href = http://sep2015.archive.ensembl.org/Homo_sapiens/Transcript/Summary?db=core;t=' + transcript + '>' + transcript + '</a>\t' + protein + '\t' + description + '\t' + chr + '\t' + start + '\t' + end + '\t' + strand + '\t' + band + '\t' + length + '\t' + gene_name + '\t' + gene_type + '\t' + transcript_type
  l = gene + '\t' + transcript + '\t' + protein + '\t' + description + '\t' + chr + '\t' + start + '\t' + end + '\t' + strand + '\t' + band + '\t' + length + '\t' + gene_name + '\t' + gene_type + '\t' + transcript_type
  dict_data[transcript] = l
  dict_data_html[transcript] = l_html
  dict_biotype[transcript] = transcript_type



dict_accepted2 = {} # dict_accepted2 will have all transcripts classified as lncRNAs at Ensembl additionally (and these transcripts need to be expressed in a given cell line)
for id in dict_accepted.keys():
  dict_accepted2[id] = ''

for id in dict_seq2.keys():
  class_code = dict_class_codes[id]
  if id in dict_nearest_ref:
    nearest_ref = dict_nearest_ref[id]
    biotype = dict_biotype[nearest_ref]
    #if class_code == '=' and biotype in ['3prime_overlapping_ncrna', 'antisense', 'lincRNA', 'retained_intron', 'sense_intronic', 'sense_overlapping', 'macro_lncRNA']:
    if class_code == '=' and biotype in ['retained_intron', 'sense_intronic', 'sense_overlapping', 'antisense_RNA', 'antisense', 'lincRNA', 'macro_lncRNA', 'bidirectional_promoter_lncRNA', 'lncRNA']:
      dict_accepted2[id] = ''




print "Preparing output files..."
passed = 0


for id, seq in dict_seq2.items():
  if id in dict_accepted2:
    write = '>' + id + '\n' + seq + '\n'
    if id in dict_TPM.keys():
      out1.write(write)





write = '#\tTranscript\tExperiment\tChr\tStart\tEnd\tStrand\tTPM\tFPKM\tLength\tNo. of exons\tClass code\tCoding potential\tExon coordinates\tNearest ref.\t' + 'Ensembl Gene\tEnsembl Transcript\tEnsembl Protein\tDescription\tChr\tStatr\tEnd\tStrand\tBand\tLength\tGene Name\tGene type\tTranscript type\n'
out2.write(write)
c = 0
passed=0
for transcript in dict_accepted2.keys():
  if transcript in dict_TPM.keys():
    c += 1
    tpm = dict_TPM[transcript]
    fpkm = 'unknown' #dict_FPKM[transcript]
    experiment = experiment.split('_')[0]
    length = dict_length[transcript]
    number_of_exons = dict_exons[transcript]
    class_code = dict_class_codes[transcript]
    chr, start, end, strand = dict_transcript_coordinates[transcript]
    exon_coordinates = dict_exon_coordinates[transcript]
    exon_coordinates = ', '.join(exon_coordinates)
    if transcript in dict_accepted2 and not transcript in dict_accepted:
      coding_potential = 'High'
    else:
      coding_potential = 'Low'
    if transcript in dict_nearest_ref:
      nearest_ref = dict_nearest_ref[transcript]
      ensembl_data = dict_data[nearest_ref]
      ensembl_data_html = dict_data_html[nearest_ref]
    else:
      nearest_ref, ensembl_data, ensembl_data_html = '', '', ''
    write = str(c) + '\t' + transcript + '\t' + experiment + '\t' + chr + '\t' + start + '\t' + end + '\t' + strand + '\t' + tpm + '\t' + fpkm + '\t' + str(length) + '\t' + str(number_of_exons) + '\t' + class_code + '\t' + coding_potential + '\t' + exon_coordinates + '\t' + nearest_ref + '\t' + ensembl_data + '\n'
    out2.write(write)
    passed+=1

    

f3.close()
f3 = open(GTF)

for line in f3:
  if not line[0] == '#':
    l = line
    line = line.strip().split('\t')
    features = line[8].split('; ')
    for elem in features:
      if 'transcript_id' in elem:
        transcript = elem.split(' ')[1].replace('"', '')
    if transcript in dict_accepted2:
      if transcript in dict_TPM.keys():
        out3.write(l)
print(passed, " transcripts passed the filtering")
