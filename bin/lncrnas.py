import sys, os

experiment = sys.argv[1]
main_fa = sys.argv[2]
main_gtf = sys.argv[3]
exp = experiment

cmd = 'mkdir PREDICTIONS/' + exp
try:
  os.system(cmd)
except:
  pass

cmd = "mkdir HELPER/" + experiment
os.system(cmd)

cmd = "python gtf2seq.py " + str(main_fa) + " " + experiment + ".gtf " + experiment + ".fasta gene2transcript/" + exp + "_gene2transcript"
os.system(cmd)

cmd = "cuffcompare -r " + str(main_fa) + " -R -o Cuffcompare/" + exp + " -C -G " + experiment + ".gtf"
os.system(cmd)

cmd = "python filter.py " + experiment
os.system(cmd)

cmd = "TransDecoder-5.0.1/TransDecoder.LongOrfs -t FASTA/" + experiment + "_pre-filtered.fasta -m 100 -S"
os.system(cmd)

cmd = "mv " + experiment + "_pre-filtered.fasta.transdecoder_dir TRANSDECODER/"
os.system(cmd)

cmd = "python transdecoder_filter.py " + experiment
os.system(cmd)


cmd = "cpc-master/bin/run_predict.sh FASTA/" + experiment + "_pre-filtered_2.fasta HELPER/" + experiment + "/result_" + experiment  + " HELPER/" + experiment + " HELPER/" + experiment + "/evidence_" + experiment

os.system(cmd)

cmd = "python candidates.py " + experiment
os.system(cmd)
