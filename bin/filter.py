#!/usr/bin/env python2.7
import sys

experiment = sys.argv[1]

length_threshold = 200 # nt (this or higher required)

GTF = sys.argv[2]
FASTA = sys.argv[3]
COMBINED = sys.argv[4]
BIOTYPES = sys.argv[5]

f1 = open(GTF)
f2 = open(FASTA)
f3 = open(COMBINED)
f4 = open(BIOTYPES)

out1 = open('FASTA/' + experiment + '_pre-filtered.fasta', 'w')
out2 = open('ADDITIONAL/' + experiment + '_reasons_discarded.txt', 'w')
out3 = open('ADDITIONAL/' + experiment + '_data.txt', 'w')


print "Reading comparison with known annotation..."
dict_exons = {}
dict_class_codes = {}
dict_nearest_ref = {}
dict_length = {}
for line in f3:
  line = line.strip().split('\t')
  features = line[8].split('; ')
  exon_start = int(line[3])
  exon_end = int(line[4])
  exon_length = exon_end - exon_start + 1
  class_code, transcript, nearest_ref = 'NA', 'NA', 'NA'
  for feature in features:
    if 'class_code' in feature:
      class_code = feature.split(' ')[1].replace('"', '')
    if 'oId' in feature:
      transcript = feature.split(' ')[1].replace('"', '')
    if 'nearest_ref' in feature:
      nearest_ref = feature.split(' ')[1].replace('"', '')
      dict_nearest_ref[transcript] = nearest_ref
  dict_class_codes[transcript] = class_code
  if transcript in dict_exons:
    dict_exons[transcript] += 1
  else:
    dict_exons[transcript] = 1
  if transcript in dict_length:
    dict_length[transcript] += exon_length
  else:
    dict_length[transcript] = exon_length

print "Reading biotypes..."
dict_biotypes = {}
f4.readline()
for line in f4:
  line = line.strip().split('\t')
  transcript = line[0]
  biotype = line[1]
  dict_biotypes[transcript] = biotype
    
print "Applying filters..."
class_codes_discard = ['c', 'e', 'p', 's']
class_codes_conditional = ['=', 'j', 'o']
biotypes_discard = ['rRNA', 'IG_C_pseudogene', 'protein_coding', 'IG_V_gene', 'polymorphic_pseudogene', 'misc_RNA', 'scaRNA', 'IG_J_gene', 'TR_J_pseudogene', 'IG_J_pseudogene', 'TEC', 'Mt_tRNA', 'Mt_rRNA', 'TR_V_pseudogene', 'TR_J_gene', 'TR_D_gene', 'IG_V_pseudogene', 'nonsense_mediated_decay', 'snRNA', 'sRNA', 'TR_V_gene', 'miRNA', 'IG_C_gene', 'ribozyme', 'IG_D_gene', 'TR_C_gene', 'snoRNA', 'vaultRNA', 'non_stop_decay']

dict_discard = {}
for transcript in dict_length.keys():
  #expression = float(dict_expression[transcript])
  class_code = dict_class_codes[transcript]
  number_of_exons = dict_exons[transcript]
  transcript_length = dict_length[transcript]
  # class codes filtering
  if class_code in class_codes_discard:
    if transcript in dict_discard:
      dict_discard[transcript].append('class_code')
    else:
      dict_discard[transcript] = ['class_code']
  if class_code in class_codes_conditional:
    nearest_ref = dict_nearest_ref[transcript]
    biotype = dict_biotypes[nearest_ref]
    if biotype in biotypes_discard:
      if transcript in dict_discard:
        dict_discard[transcript].append('biotype')
      else:
        dict_discard[transcript] = ['biotype']
  if transcript_length < 200:
    if transcript in dict_discard:
      dict_discard[transcript].append('length')
    else:
      dict_discard[transcript] = ['length']

print "Writing output files..."
dict_seq = {}
for line in f2:
  if line[0] == '>':
    id = line[1:].split(' ')[0].strip()
  else:
    seq = line.strip()
    if not id in dict_discard:
      if id in dict_seq:
        dict_seq[id] += seq
      else:
        dict_seq[id] = seq

for id, seq in dict_seq.items():
  write = '>' + id + '\n' + seq + '\n'
  out1.write(write)

for transcript, reasons in dict_discard.items():
  reasons = ", ".join(reasons)
  write = transcript + '\t' + reasons + '\n'
  out2.write(write)

for transcript in dict_length.keys():
  expression = 'unknown' #float(dict_expression[transcript])
  class_code = dict_class_codes[transcript]
  number_of_exons = dict_exons[transcript]
  transcript_length = dict_length[transcript]
  if class_code in class_codes_conditional:
    nearest_ref = dict_nearest_ref[transcript]
    biotype = dict_biotypes[nearest_ref]
  else:
    nearest_ref, biotype = '', ''
  write = transcript + '\t' + str(expression) + '\t' + class_code + '\t' + str(number_of_exons) + '\t' + str(transcript_length) + '\t' + nearest_ref + '\t' + biotype + '\n'
  out3.write(write)

