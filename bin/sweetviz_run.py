#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#!pip install sweetviz
#import io
#from google.colab import drive
#drive.mount('/content/gdrive')

import pandas as pd
import numpy as np
import sweetviz as sv
import sys, os

# ./sweetviz_run.py report assembly hsapiens
#./sweetviz_run.py compare assembly hsapiens ecaballus
# ./sweetviz_run.py report conservation hsapiens-ecaballus
# ./sweetviz_run.py compare conservation hsapiens-ecaballus hsapiens-ptroglodytes

objective = sys.argv[1] #"report" ## "compare "
data_type = sys.argv[2] #"assembly" ## "conservation"
if data_type == "assembly":
    names1 = sys.argv[3]
    try:
        names2 = sys.argv[4] 
    except:
        print("no comparison")
elif data_type == "conservation":
    names1 = sys.argv[3] #"hsapiens-ptroglodytes"
    try:
        names2 = sys.argv[4] #"hsapiens-ecaballus"
    except:
        print("report only")




#analyzing the dataset
#lnc_report = sv.analyze(df2)
#display the report
#lnc_report.show_html('./assembly.html')


def make_df_assembly(expiriment):
    # Compare
    expiriment            
    df2=pd.read_csv(str("./"+ str(expiriment) +"_lncrnas_data.txt"),sep="\t")
    print(df2.dtypes)
    print(df2.head(3))
    duplicateRowsDF = df2[df2.duplicated()]
    print("Duplicate Rows except first occurrence based on all columns are :")
    print(duplicateRowsDF)
    df2.drop_duplicates(keep=False ,inplace=True) 
    cols_to_count_df2=["Transcript","TPM","Length","No. of exons","Class code","Coding potential","Nearest ref.", "Ensembl Gene","Ensembl Transcript","Ensembl Protein"]
    df2=df2.fillna(0)
    df2.TPM = df2.TPM.round().clip(1, 100)
    df2.Length =df2.Length.clip(1, 5000)
    to_str=["Nearest ref.", "Ensembl Gene","Ensembl Transcript","Ensembl Protein"]
    for column_id in to_str:
      df2[column_id] = df2[column_id].astype(str)
    df2 = df2[cols_to_count_df2]
    df2.head(3)
    #analyzing the dataset
    return df2

if data_type == "assembly":
    if objective == "compare":
        dict_of_dfs = dict()
        df_names = [str(names1), str(names2)]
        for name in df_names:
            print(name)
            dict_of_dfs[str(name)] = make_df_assembly(str(name))
        to_compare=[]
        for exp in dict_of_dfs:
            to_compare.append([dict_of_dfs[exp], str(exp)]) 
        df1 = sv.compare(to_compare[0],to_compare[1])
        df1.show_html('./' + str(names1) + "_" + str(names2) + '_lncRNAS.html')
    elif objective == "report":
        df_conservation = make_df_assembly(str(names1))
        df_analyzied = sv.analyze(df_conservation)
        df_analyzied.show_html('./'+ str(names1) +'_lncRNAS.html')
    else: 
        pass
else:
    pass


def make_df(expiriment):
    # Compare
    expiriment            
    df=pd.read_csv(str("./" + expiriment.split("-")[0] + "." + expiriment.split("-")[1] + ".orthologs.top.final.txt"),sep="\t")
    duplicateRowsDF = df[df.duplicated()]
    df.drop_duplicates(keep=False ,inplace=True)
    df = df.rename(columns={str("category(" + str(expiriment.split("-")[0]) +")"): 'category_target', str("category(" + str(expiriment.split("-")[1])+ ")"): 'category_query'})
    print(df)
    cols_to_count=["#lnc","exonID","locusID","indelRate(exon)","indelRate(intron)","spliceConserved","spliceTotal", "category_target", "category_query"]
    df=df.fillna(0)
    df = df[cols_to_count]
    to_float=["exonID","locusID","indelRate(exon)","indelRate(intron)","spliceConserved","spliceTotal"]
    for column_id in to_float:
      df[column_id] = df[column_id].astype(float)
    df['category'] = df.iloc[:,-2].str.cat(df.iloc[:,-1],sep="-")
    return df

if data_type == "conservation":
    if objective == "compare":
        dict_of_dfs = dict()
        df_names = [str(names1), str(names2)]
        for name in df_names:
            print(name)
            dict_of_dfs[str(name)] = make_df(str(name))
        to_compare=[]
        for exp in dict_of_dfs:
            to_compare.append([dict_of_dfs[exp], str(exp)]) 
        df1 = sv.compare(to_compare[0],to_compare[1])
        df1.show_html('./' + str(names1) + "_" + str(names2) + '_conservation.html')
    elif objective == "report":
        df_conservation = make_df(str(names1))
        df_analyzied = sv.analyze(df_conservation)
        df_analyzied.show_html('./' + str(names1) + '_conservation.html')
    else: 
        pass
else:
    pass
