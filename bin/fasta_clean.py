#!/usr/bin/env python3
import sys
from Bio import SeqIO

fasta_file = sys.argv[1]
fasta_file_out = sys.argv[2]


valid_sequences = []
for record in SeqIO.parse(fasta_file, "fasta"):
    if len(record.id) < 5 or str(record.id) == "MT":
        valid_sequences.append(record)

SeqIO.write(valid_sequences, fasta_file_out, "fasta")
