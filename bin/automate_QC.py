import glob, os, sys 

directory = sys.argv[1]


cwd = os.getcwd()
#print(cwd) /home/root/Data/horse

samples = []
for file in glob.glob(directory + '/*.fastq.gz'):
  sample = file.split('/')[-1].replace('.fastq.gz', '').replace('_R1', '').replace('_R2', '')
  samples.append(sample)

samples = set(samples)

for sample in samples:
  file1 = sample + '_R1.fastq.gz'
  file2 = sample + '_R2.fastq.gz'

  cmd = 'mkdir FASTQC_out/' + sample
  os.system(cmd)

  cmd = '/main/FastQC/fastqc ' +  str(cwd) + '/reads/' + file1  + ' ' + str(cwd) + '/reads/' + file2 + ' --outdir FASTQC_out/' + sample 
  os.system(cmd)

  cmd = '/main/bbmap/bbduk2.sh -Xmx2g threads=2 in=' + str(cwd) + '/reads/#sample#_R1.fastq.gz in2=' + str(cwd) + '/reads/#sample#_R2.fastq.gz out=TRIMMED/#sample#_R1.fastq out2=TRIMMED/#sample#_R2.fastq qtrim=w trimq=20 maq=10 rref=/main/bbmap/resources/adapters.fa k=23 mink=11 hdist=1 tbo tpe minlength=50 removeifeitherbad=t overwrite=t stats=STATUS/#sample#.bbduk2_stats.txt 2> STATUS/#sample#.bbduk2_trimming.txt'
  cmd = cmd.replace('#sample#', sample)
  os.system(cmd)



