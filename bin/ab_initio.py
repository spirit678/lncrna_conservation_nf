import glob, os, sys

gtf_file = sys.argv[1]

samples = []
for file in glob.glob('TRIMMED/*.fastq'):
  sample = file.split('/')[-1].replace('_R1', '').replace('_R2', '').replace('.fastq', '').replace('_clean', '')
  samples.append(sample)

samples = set(samples)
samples = list(samples)
 

for sample in samples:

  cmd = "/main/STAR-2.7.3a/bin/Linux_x86_64_static/STAR --runThreadN 4 --limitBAMsortRAM 19261643698 --genomeDir genome_index --readFilesIn TRIMMED/#experiment#_clean_R1.fastq TRIMMED/#experiment#_clean_R2.fastq --outFileNamePrefix star/#experiment# --outSAMattributes All --outSAMattrIHstart 0 --outSAMtype BAM SortedByCoordinate --outSAMunmapped Within --outSAMstrandField intronMotif --outFilterIntronMotifs RemoveNoncanonical --outFilterType BySJout --outFilterMultimapNmax 20 --alignSJoverhangMin 8 --alignSJDBoverhangMin 1 --outFilterMismatchNmax 999 --outFilterMismatchNoverLmax 0.04 --alignIntronMin 20 --alignIntronMax 1000000 --alignMatesGapMax 1000000 --twopassMode Basic --chimSegmentMin 12 --chimJunctionOverhangMin 12 --chimSegmentReadGapMax 3 > STATUS/#experiment#_STAR.txt"
  cmd = cmd.replace("#experiment#", sample)
  os.system(cmd)

  cmd = "/main/stringtie/stringtie star/#experiment#Aligned.sortedByCoord.out.bam -o GTF/#experiment#.gtf -p 4 -G " + str(gtf_file) + " -A #experiment#_abundance.txt > STATUS/#experiment#_stringtie.txt"
  cmd = cmd.replace("#experiment#", sample)
  os.system(cmd)





