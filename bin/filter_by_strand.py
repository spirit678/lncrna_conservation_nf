#!/usr/bin/env python2.7
import sys
input = sys.argv[1]
out = open('transcriptome_pre.gtf', 'w')


f = open(input)

f.readline()
f.readline()
for line in f:
  l = line
  line = line.strip().split('\t')
  # class code and transcript_id info is held in the 8th element of the line
  features = line[8].split('; ')
  class_code, transcript_id, gene_id = '', '', ''
  for elem in features:
    if 'transcript_id' in elem:
      transcript_id = elem.split(' ')[1].replace('"', '')
    if 'gene_id' in elem:
      gene_id = elem.split(' ')[1].replace('"', '')
  strand = line[6]
  if strand in ['+', '-']:
    out.write(l)


  
  
