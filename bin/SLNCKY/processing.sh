#/bin/sh

species=$1
species2=$2
threads=$3
minM=$5
pad=$4
r=$(pwd)
echo "Workdir is $r"
echo "$species"
lnc_FOLDER="./lnc_pool/*"
i=0
j=$(ls -1 ./lnc_pool/ | wc -l)
#grub="python ./RUN.py -o $sp --threads $threads"
grub="python2.7 ./slncky.v1.0 --no_filter --minMatch=$minM --pad=$pad --config ./annotations.config --threads=$threads ./transcripts_all.bed $species $species2"
for f in $lnc_FOLDER
do
    i=$((i + 1))
    echo "$i from $j"
    cp $f ./transcripts_all.bed
    eval $grub
    sleep 10s
    mkdir ./results/$species"_"$i
    mv ./$species2".orfs.txt" ./results/$species"_"$i
    mv ./$species2".orthologs.top.txt" ./results/$species"_"$i
    mv ./$species2".orthologs.txt" ./results/$species"_"$i
    sleep 30s
done
echo "concatinating results wait 10 sec"
sleep 10s
orf=$species2".orfs.txt"
top=$species2".orthologs.top.txt"
all=$species2".orthologs.txt"
for file in ./results/$species"_"*/*
do
	filename="${file##*/}"
	echo $filename
	if [ $filename = $orf ]; then
		cat $file >> ./results/$orf
	elif [ $filename = $top ]; then
		cat $file >> ./results/$top
	elif [ $filename = $all ]; then
		cat $file >> ./results/$all
	else
  		echo "achtung !!!"
	fi
done
echo "Mission $species complite"
