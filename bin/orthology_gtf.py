#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 27 13:55:36 2017

@author: spirit678
"""


import os
import subprocess
import sys

gtf = "./" + sys.argv[1]
data = "./" + sys.argv[2]
output = "./" + sys.argv[3] + "_formatted.gtf" 

def michal_to_ensembl(gtf, data, output):
    dict_transcript = {}
    f = open(gtf, "r") 
    f2 = open(data, "r")
    out = open(output, "w")
    for line in f:
      if not line[0] == '#':
        line = line.strip().split('\t')
        if line[2] == 'exon':
          features = line[8]
          transcript_id = features.split('; ')[1]
          gene_id = features.split('; ')[0]
          gID = gene_id.split(' ')[1].replace('"', '')
          TID = transcript_id.split(' ')[1].replace('"', '')
          exon_id = features.split('; ')[2]
          feature = gene_id + '; gene_version "1"; ' + transcript_id + '; transcript_version "1"; ' + exon_id + ' gene_source "cufflinks";  ' + 'gene_biotype "lncRNA_from_predictions"; transcript_source "Cufflinks"; transcript_biotype "lncRNA_from_predictions"; exon_version "1";'
          write = line[0] + '\t' + line[1] + '\t' + line[2] +'\t'  + line[3]  + '\t' + line[4] + '\t' + line[5] +'\t' + line[6] +'\t' + line[7] +'\t' + feature + '\n' 
          out.write(write)
          if TID not in dict_transcript:
              dict_transcript[TID] = [gID]
          else:
              pass
    #print(dict_transcript.items())          
    for line in f2:
      if not line[0] == '#':
        line = line.strip().split('\t')        
        features = line[8]
        transcript_id = line[1]
        if transcript_id in dict_transcript:        
            gene_id = dict_transcript.get(transcript_id)
            gene_id ="gene_id" + ' "' + gene_id[0] + '"'
            transcript_id  ="transcript_id" + ' "' + transcript_id + '"'
            #print(gene_id)
        else:
            #print(transcript_id)
            pass
        feature = str(gene_id) + '; gene_version "1"; ' + str(transcript_id) + '; transcript_version "1"; ' + 'gene_source "cufflinks";  ' + 'gene_biotype "lncRNA_from_predictions"; transcript_source "Cufflinks"; transcript_biotype "lncRNA_from_predictions";'
        write2 = line[3] + '\t' + "Cufflinks" + '\t' + "transcript"  + '\t' + line[4]  + '\t' + line[5] + '\t' + "." + '\t' + line[6]  + '\t' + "."  + '\t' + feature + '\n' 
        #print(transcript_id)
        out.write(write2)
    out.close





           
#Running
michal_to_ensembl(gtf, data, output)
print("Formatting of lncRNAs..Done")     

    

