#!/usr/bin/env python3
import glob, os, sys
from pybiomart import Server
import pandas as pd
from ftplib import FTP

ensembl_name = sys.argv[1]


#Ensembl mart
server = Server(host='http://www.ensembl.org')
version = server.list_marts()
version = version[version['display_name'].str.contains('Ensembl Genes')]
version = str(version.iloc[0]['display_name']).split(" ")[2]
print("Ensembl version " + version)
mart = server['ENSEMBL_MART_ENSEMBL'] 
df = mart.list_datasets()
df2 = df[df['name'].str.contains(str(ensembl_name))]
if df2.empty == True:
    df2 = df[df['display_name'].str.contains(str(ensembl_name))]
print("Assembly version " + str(df2.iloc[0]['display_name']).split("(")[1].split(")")[0])



#Download gtf and toplevel.fa
import urllib 
urllib.request.urlretrieve(str("ftp://ftp.ensembl.org/pub/release-" + version + "/species_EnsemblVertebrates.txt"), './ensembl_release.tsv')
df_temp = pd.read_csv('./ensembl_release.tsv', sep='\t', index_col=False)
df_temp = df_temp[df_temp['assembly'].str.contains(str(df2.iloc[0]['display_name']).split("(")[1].split(")")[0])]
#print(str(df_temp["species"].values[0]))
#Fasta

ftp = FTP("ftp.ensembl.org")
ftp.login()
filepath = "/pub/release-" + str(version) + "/fasta/" + str(df_temp["species"].values[0]) + "/dna/"
ftp.cwd(filepath)
ftp.retrlines('LIST *dna.toplevel.fa.gz*')
target_dir='./'
filematch='*.dna.toplevel.fa.gz'
filematch_primary='*.dna.primary_assembly.fa.gz'

if not ftp.nlst(filematch_primary):
	for filename in ftp.nlst(filematch):
		target_file_name = os.path.join(target_dir,os.path.basename(filename))
		print(str(target_file_name))
		with open("genome.dna.toplevel.fa.gz",'wb') as fhandle:
			ftp.retrbinary('RETR %s' %filename, fhandle.write)

else:

	for filename in ftp.nlst(filematch_primary):
		target_file_name = os.path.join(target_dir,os.path.basename(filename))
		print(str(target_file_name))		
		with open("genome.dna.toplevel.fa.gz",'wb') as fhandle:
			ftp.retrbinary('RETR %s' %filename, fhandle.write)	


#GTF
ftp = FTP("ftp.ensembl.org")
ftp.login()
filepath = "/pub/release-" + str(version) +"/gtf/" + str(df_temp["species"].values[0]) + "/"
ftp.cwd(filepath)
ftp.retrlines('LIST *dna.toplevel.fa.gz*')
target_dir='./'
filematch='*.gtf.gz'
for filename in ftp.nlst(filematch):
	target_file_name = os.path.join(target_dir,os.path.basename(filename))
	if "chr" not in str(target_file_name):
		if "abinitio" not in str(target_file_name):
			#print(str(target_file_name))
			with open("genes.gtf.gz",'wb') as fhandle:
				ftp.retrbinary('RETR %s' %filename, fhandle.write)
#unzip everything
"""


wget ftp://ftp.ensembl.org/pub/release-97/fasta/equus_caballus/dna/Equus_caballus.EquCab3.0.dna.toplevel.fa.gz
gunzip Equus_caballus.EquCab3.0.dna.toplevel.fa.gz
wget ftp://ftp.ensembl.org/pub/release-97/gtf/equus_caballus/Equus_caballus.EquCab3.0.97.gtf.gz
gunzip Equus_caballus.EquCab3.0.97.gtf.gz


"""
