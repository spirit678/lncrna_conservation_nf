#!/usr/bin/env python3
import os
#import csv
import sys
chrom= {} 


input_f = sys.argv[1]
output_f = sys.argv[2]


out=open(output_f, "w")
f = open(input_f, "r")
for line in f:
	if line[0] == '>':
		id = line[1:].split(' ')[0].strip()
		if len(id) < 3 and id != 'MT':
			id = 'chr' + id
		line = '>' + id + '\n'
	out.write(line)
out.close
f.close  
