#!/usr/bin/env python2.7
import sys
f1 = open(sys.argv[1]) #'all.combined.gtf')
f2 = open(sys.argv[2])#'transcriptome_pre.gtf')
out = open(sys.argv[3], 'w')

dict_class_codes = {}
for line in f1:
  line = line.strip().split('\t')
  features = line[8].split('; ')
  transcript_id, class_code = '', ''
  for elem in features:
    if 'oId ' in elem:
      transcript_id = elem.split(' ')[1].replace('"', '')
    if 'class_code ' in elem:
      class_code = elem.split(' ')[1].replace('"', '')
  if transcript_id and class_code:
    dict_class_codes[transcript_id] = class_code
      

for line in f2:
  l = line
  line = line.strip().split('\t')
  features = line[8][:-1].split('; ')
  transcript_id, class_code = '', ''
  for elem in features:
    if 'transcript_id ' in elem:
      transcript_id = elem.split(' ')[1].replace('"', '')
  if transcript_id:
    if transcript_id in dict_class_codes:
      if not dict_class_codes[transcript_id] in ['c', 'e', 'p', 's']:
        out.write(l)
    else:
      if 'ENS' in transcript_id:
        out.write(l)
    if not transcript_id in dict_class_codes and not 'ENS' in transcript_id:
      print transcript_id
