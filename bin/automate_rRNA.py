import glob, os

samples = []
for file in glob.glob('TRIMMED/*.fastq'):
  sample = file.split('/')[-1].replace('_R1', '').replace('_R2', '').replace('.fastq', '')
  samples.append(sample)

samples = set(samples)

for sample in samples:
  cmd = "bowtie2 -t -p 4 -X 1000 -1 TRIMMED/#sample#_R1.fastq -2 TRIMMED/#sample#_R2.fastq -x index/rRNAs --fast --un-conc #sample#.fastq > /dev/null"
  cmd = cmd.replace("#sample#", sample)
  os.system(cmd)

  cmd = "mv #sample#.1.fastq TRIMMED/#sample#_clean_R1.fastq"
  cmd = cmd.replace("#sample#", sample)
  os.system(cmd)

  cmd = "mv #sample#.2.fastq TRIMMED/#sample#_clean_R2.fastq"
  cmd = cmd.replace("#sample#", sample)
  os.system(cmd)

  cmd = 'rm TRIMMED/' + sample + '_R1.fastq'
  os.system(cmd)

  cmd = 'rm TRIMMED/' + sample + '_R2.fastq'
  os.system(cmd)


