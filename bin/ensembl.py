#!/usr/bin/env python
import glob, os, sys
from pybiomart import Server
import pandas as pd
from ftplib import FTP

ensembl_name = sys.argv[1]


#Ensembl mart
server = Server(host='http://www.ensembl.org')
version = server.list_marts()
version = version[version['display_name'].str.contains('Ensembl Genes')]
version = str(version.iloc[0]['display_name']).split(" ")[2]
print("Ensembl version " + version)
mart = server['ENSEMBL_MART_ENSEMBL'] 
df = mart.list_datasets()
df = df[df['display_name'].str.contains(str(ensembl_name))]

#print("Assembly version " + str(df.iloc[0]['display_name']).split("(")[1].split(")")[0])
#print(str(df.iloc[0]['name']).split("_")[0] + "_" + str(df.iloc[0]['name']).split("_")[1])
print(str(df.iloc[0]['name']))
dataset = mart[str(df.iloc[0]['name'])]
df2 = dataset.query(attributes=['ensembl_gene_id', 'external_gene_name', 'gene_biotype'], filters={'biotype': ['rRNA','rRNA_pseudogene']})
#print(df2.shape)
dfToList = df2['Gene stable ID'].tolist()
#print(dfToList)


###For lnc identification
df3 = dataset.query(attributes=['ensembl_transcript_id', 'transcript_biotype'])
#print(df3.shape)
df3.to_csv('./transcript_biotypes',sep='\t', index=False)
df4 = dataset.query(attributes=['ensembl_gene_id', 'ensembl_transcript_id', 'ensembl_peptide_id', 'description', 'chromosome_name', 'strand','band','transcript_start','transcript_end','transcript_length',"external_gene_name", 'gene_biotype','transcript_biotype'])
#print(df4.shape)
df4.to_csv('./ensembl_data.txt',sep='\t', index=False)



#Download rRNA fasta
import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

def requests_retry_session(
    retries=3,
    backoff_factor=0.3,
    status_forcelist=(500, 502, 504, 503),
    session=None,
):
    session = session or requests.Session()
    retry = Retry(
        total=retries,
        read=retries,
        connect=retries,
        backoff_factor=backoff_factor,
        status_forcelist=status_forcelist,
    )
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    return session



fasta = open("rRNAs.fasta","w")
server = "http://rest.ensembl.org"
reqs_per_sec=15
for i in dfToList:
	ext = "/sequence/id/" + str(i) + "?type=cdna"
	r = requests_retry_session().get(server+ext, headers={ "Content-Type" : "text/x-fasta"})
	if not r.ok:
		r.raise_for_status()
		sys.exit()
	fasta.write( r.text + '\n')
fasta.close()


#Download gtf and toplevel.fa
import urllib 
urllib.request.urlretrieve(str("ftp://ftp.ensembl.org/pub/release-" + version + "/species_EnsemblVertebrates.txt"), './ensembl_release.tsv')
### ENSEMBL relese 103 added new column
new_columns=["#name","species","division","taxonomy_id","assembly","assembly_accession","genebuild","variation","pan_compara","peptide_compara", "genome_alignments","other_alignments","core_db","species_id","new_unexpected_column"]
df_temp = pd.read_csv('./ensembl_release.tsv', sep='\t', index_col=False, names=new_columns)
df_temp = df_temp[df_temp['assembly'].str.contains(str(df.iloc[0]['display_name']).split("(")[1].split(")")[0])]
print(str(df_temp["species"].values[0]))
#Fasta

ftp = FTP("ftp.ensembl.org")
ftp.login()
filepath = "/pub/release-" + str(version) + "/fasta/" + str(df_temp["species"].values[0]) + "/dna/"
ftp.cwd(filepath)
ftp.retrlines('LIST *dna.toplevel.fa.gz*')
target_dir='./'
filematch='*.dna.toplevel.fa.gz'
filematch_primary='*.dna.primary_assembly.fa.gz'

if not ftp.nlst(filematch_primary):
	for filename in ftp.nlst(filematch):
		target_file_name = os.path.join(target_dir,os.path.basename(filename))
		print(str(target_file_name))
		with open("genome.dna.toplevel.fa.gz",'wb') as fhandle:
			ftp.retrbinary('RETR %s' %filename, fhandle.write)

else:

	for filename in ftp.nlst(filematch_primary):
		target_file_name = os.path.join(target_dir,os.path.basename(filename))
		print(str(target_file_name))		
		with open("genome.dna.toplevel.fa.gz",'wb') as fhandle:
			ftp.retrbinary('RETR %s' %filename, fhandle.write)	

#GTF
ftp = FTP("ftp.ensembl.org")
ftp.login()
filepath = "/pub/release-" + str(version) +"/gtf/" + str(df_temp["species"].values[0]) + "/"
ftp.cwd(filepath)
ftp.retrlines('LIST *dna.toplevel.fa.gz*')
target_dir='./'
filematch='*.gtf.gz'
for filename in ftp.nlst(filematch):
	target_file_name = os.path.join(target_dir,os.path.basename(filename))
	if "chr" not in str(target_file_name):
		if "abinitio" not in str(target_file_name):
			#print(str(target_file_name))
			with open("genes.gtf.gz",'wb') as fhandle:
				ftp.retrbinary('RETR %s' %filename, fhandle.write)
#unzip everything
"""


wget ftp://ftp.ensembl.org/pub/release-97/fasta/equus_caballus/dna/Equus_caballus.EquCab3.0.dna.toplevel.fa.gz
gunzip Equus_caballus.EquCab3.0.dna.toplevel.fa.gz
wget ftp://ftp.ensembl.org/pub/release-97/gtf/equus_caballus/Equus_caballus.EquCab3.0.97.gtf.gz
gunzip Equus_caballus.EquCab3.0.97.gtf.gz


"""
