FROM continuumio/miniconda3:4.7.12
LABEL authors="olebry@amu.edu.pl" \
      description="Docker image containing all requirements for the lncRNA conservation pipe"
RUN apt-get update && apt-get install -y procps && apt-get clean -y
RUN conda create -n sweetviz python=3.6  && conda clean -a
COPY cpc.yml /
RUN conda env create -f /cpc.yml && conda clean -a
COPY environment.yml /
RUN conda env create -f /environment.yml && conda clean -a
ENV PATH /opt/conda/envs/nf-core-rnaseq/bin:$PATH
